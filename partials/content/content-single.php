<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'mb-4 pb-4' ); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<figure title="<?php the_title_attribute(); ?>">
			<?php
			the_post_thumbnail( 'medium_large', array(
				'class' => 'post-thumbnail img-fluid',
				'title' => get_the_title()
			) );
			?>
		</figure>
		<p>
			<i class="dashicons dashicons-admin-users"></i>
			<span><?php the_author_posts_link(); ?>&nbsp;</span>

			<i class="dashicons dashicons-calendar-alt"></i>
			<span><?php echo get_the_date(); ?>&nbsp;</span>

			<i class="dashicons dashicons-category"></i>
			<span><?php the_category(', '); ?>&nbsp;</span>

			<?php if ( has_tag() ) : ?>
				<i class="dashicons dashicons-tag"></i>
				<span><?php the_tags('', ', ', ''); ?>&nbsp;</span>
			<?php endif; ?>

			<?php if ( get_comments_number() ) : ?>
				<i class="dashicons dashicons-admin-comments"></i>
				<span><?php echo get_comments_number(); ?></span>
			<?php endif; ?>
		</p>
	</header>
	<!-- /.entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->