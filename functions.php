<?php

/**
 * 
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// 
function nhrd_scripts() {
    // CSS
    wp_enqueue_style( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css', null, '4.5.0', 'all' );
    wp_enqueue_style( 'nhrd-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'nhrd-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'nhrd-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get( 'Version' ) );

    // Js
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//code.jquery.com/jquery-3.4.1.min.js', array(), '3.4.1', true );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script('comment-reply');
    wp_enqueue_script( 'popper', '//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array( 'jquery' ), '1.16.0', true );
    wp_enqueue_script( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array( 'jquery', 'popper' ), '4.5.0', true );
    wp_enqueue_script( 'nhrd-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'nhrd_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function nhrd_gutenberg_scripts() {
    wp_enqueue_style( 'nhrd-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_script('nhrd-editor-script', get_template_directory_uri() . '/assets/js/admin/editor.js', array('wp-blocks', 'wp-dom'), '1.0', true);
}

add_action('enqueue_block_editor_assets', 'nhrd_gutenberg_scripts');

/** 
 * Set theme defaults and register support for various WordPress features.
 */
function nhrd_setup() {
    // Enabling translation support
    $textdomain = 'nhrd';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 61,
        'width'       => 220,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Menu registration
    register_nav_menus(array(
        'topbar_menu_left' => __( 'Topbar Left', 'nhrd' ),
        'topbar_menu_right' => __( 'Topbar Right', 'nhrd' ),
        'main_menu' => __( 'Main Menu', 'nhrd' ),
        'footer_menu' => __( 'Footer Menu', 'nhrd' ),
    ));

    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/admin/admin-styles.css' );    

    // Enables the editor's dark mode
    add_theme_support( 'dark-editor-style' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
	add_theme_support( 'wp-block-styles' );
}

add_action( 'after_setup_theme', 'nhrd_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nhrd_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #1', 'nhrd' ),
        'id' => 'nhrd-sidebar-footer-1',
        'description' => __( 'The widgets in this area will be displayed in the first column in Footer.', 'nhrd' ),
    ) ) );

    // Footer #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #2', 'nhrd' ),
        'id' => 'nhrd-sidebar-footer-2',
        'description' => __( 'The widgets in this area will be displayed in the second column in Footer.', 'nhrd' ),
    ) ) );

    // Footer #3
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #3', 'nhrd' ),
        'id' => 'nhrd-sidebar-footer-3',
        'description' => __( 'The widgets in this area will be displayed in the third column in Footer.', 'nhrd' ),
    ) ) );

    // Footer #4
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #4', 'nhrd' ),
        'id' => 'nhrd-sidebar-footer-4',
        'description' => __( 'The widgets in this area will be displayed in the fourth column in Footer.', 'nhrd' ),
    ) ) );

    // Barra Lateral Blog #1
    register_sidebar( array_merge( $shared_args, array (
        'name' => __( 'Sidebar Blog', 'nhrd' ),
        'id' => 'nhrd-sidebar-blog',
        'description' => __( 'The widgets in this area will be displayed in the blog sidebar.', 'nhrd' ),
    ) ) );
}

add_action( 'widgets_init', 'nhrd_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 * Custom Post-Types
 */
require_once get_template_directory() . '/includes/testimonials-post-types.php';

/**
 *  Theme Custom Shortcodes
 */
require_once get_template_directory() . '/includes/custom-shortcodes.php';

/**
 *  Theme Customizer
 */
require_once get_template_directory() . '/includes/customizer.php';