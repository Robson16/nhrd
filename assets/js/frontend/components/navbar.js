export default class Navbar {
    constructor() {
        this.wpadminbar = document.querySelector("#wpadminbar");
        this.topbar = document.querySelector("#topbar");
        this.navbar = document.querySelector("#navbar");
        this.viwewportX = document.documentElement.clientWidth;
        this.offset = this.topbar.offsetHeight;
        this.events();
    }

    // Events
    events() {
        window.addEventListener("scroll", () => {
            this.handleStickEffect();
            this.handleWhenLoggedIn();
        });

        this.handleWindowResize();
    }

    // Methods
    handleStickEffect() {
        if (window.scrollY > this.offset) {
            this.navbar.classList.add("sticky");
        } else {
            this.navbar.classList.remove("sticky");
        }
    }

    handleWhenLoggedIn() {
        // When the WP Admin Bar is visible, we need to change the top space to stick the navbar menu visible below it
        if (window.scrollY > this.offset) {
            if (this.wpadminbar && this.viwewportX > 576) this.navbar.style.top = this.wpadminbar.offsetHeight + "px";
        } else {
            if (this.wpadminbar && this.viwewportX > 576) this.navbar.style.top = "initial";
        }
    }

    handleWindowResize() {
        window.addEventListener("resize", () => {
            this.viwewportX = document.documentElement.clientWidth;
        });
    }
}