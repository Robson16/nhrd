<?php // Sidebar Blog ?>

<aside class="blog-sidebar">
    <?php if ( is_active_sidebar( 'nhrd-sidebar-blog' ) ) dynamic_sidebar( 'nhrd-sidebar-blog' ); ?>
</aside>