<?php
/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

// Gets to custom colors
$footerBgColor = get_theme_mod('nhrd_customizer_footer_bgcolor');
if (!$footerBgColor) $footerBgColor = "#3E393B";

$footerTxtColor = get_theme_mod('nhrd_customizer_footer_txtcolor');
if (!$footerTxtColor) $footerTxtColor = "#FFFFFF";
?>

<footer 
    class="footer"
    style="<?php
        echo "background-color: " . $footerBgColor . ";";
        echo "color: " . $footerTxtColor . ";";
    ?>"
>
    <div class="footer-widgets">
        <div class="container">        
            <?php if ( is_active_sidebar( 'nhrd-sidebar-footer-1' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'nhrd-sidebar-footer-1' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'nhrd-sidebar-footer-2' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'nhrd-sidebar-footer-2' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'nhrd-sidebar-footer-3' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'nhrd-sidebar-footer-3' ); ?> 
                </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'nhrd-sidebar-footer-4' ) ) : ?>
                <div class="widget-column"> 
                    <?php dynamic_sidebar( 'nhrd-sidebar-footer-4' ); ?> 
                </div>
            <?php endif; ?>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.footer-widgets -->

    <div class="footer-copyright">
        <div class="container">
            <span>&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<strong><?php echo bloginfo('title'); ?></strong>&nbsp;<?php _e('All rights reserved.', 'nhrd') ?></span>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'footer_menu',
                'depth' => 1,
                'container_class' => 'footer-menu-wrap',
                'menu_class' => 'footer-menu',
            ) );
            ?>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.footer-copyright -->
</footer>   

<?php wp_footer(); ?>

</body>

</html>