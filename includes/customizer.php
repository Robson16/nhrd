<?php

/**
 * Create customizer areas
 *
 **/
function nhrd_customizer($wp_customize) {

    // Colors Customizer
    $wp_customize->add_section(
        'nhrd_customizer_colors',
        array(
            'title' => esc_html__( 'Colors', 'nhrd' ),
            // 'priority' => 150
        )
    );

    // Footer Background Color
    $wp_customize->add_setting( 
        'nhrd_customizer_footer_bgcolor', 
        array(
            'default' => '#3E393B',
            'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
        )
    );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control(
        $wp_customize,
        'nhrd_customizer_footer_bgcolor',
            array(
                'label'      => __( 'Footer Background Color', 'nhrd' ),
                'section'    => 'nhrd_customizer_colors'
            )
        ) 
    );

    // Footer Text Color
    // Footer Background Color
    $wp_customize->add_setting(
        'nhrd_customizer_footer_txtcolor',
        array(
            'default' => '#FFFFFF',
            'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
        $wp_customize,
        'nhrd_customizer_footer_txtcolor',
            array(
                'label'      => __( 'Footer Text Color', 'nhrd' ),
                'section'    => 'nhrd_customizer_colors'
            )
        ) 
    );

}

add_action('customize_register', 'nhrd_customizer');
