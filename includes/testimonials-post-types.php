<?php

// Register Custom Post Type
function nhrd_testimonials() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'nhrd' ),
        'singular_name'         => _x( 'Testimony', 'Post Type Singular Name', 'nhrd' ),
        'menu_name'             => __( 'Testimonials', 'nhrd' ),
        'name_admin_bar'        => __( 'Testimonials', 'nhrd' ),
        'archives'              => __( 'Item Archives', 'nhrd' ),
		'attributes'            => __( 'Item Attributes', 'nhrd' ),
		'parent_item_colon'     => __( 'Parent Item:', 'nhrd' ),
		'all_items'             => __( 'All Items', 'nhrd' ),
		'add_new_item'          => __( 'Add New Item', 'nhrd' ),
		'add_new'               => __( 'Add New', 'nhrd' ),
		'new_item'              => __( 'New Item', 'nhrd' ),
		'edit_item'             => __( 'Edit Item', 'nhrd' ),
		'update_item'           => __( 'Update Item', 'nhrd' ),
		'view_item'             => __( 'View Item', 'nhrd' ),
		'view_items'            => __( 'View Items', 'nhrd' ),
		'search_items'          => __( 'Search Item', 'nhrd' ),
		'not_found'             => __( 'Not found', 'nhrd' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'nhrd' ),
		'featured_image'        => __( 'Featured Image', 'nhrd' ),
		'set_featured_image'    => __( 'Set featured image', 'nhrd' ),
		'remove_featured_image' => __( 'Remove featured image', 'nhrd' ),
		'use_featured_image'    => __( 'Use as featured image', 'nhrd' ),
		'insert_into_item'      => __( 'Insert into item', 'nhrd' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'nhrd' ),
		'items_list'            => __( 'Items list', 'nhrd' ),
		'items_list_navigation' => __( 'Items list navigation', 'nhrd' ),
		'filter_items_list'     => __( 'Filter items list', 'nhrd' ),
    );
    $args = array(
        'label'                 => __( 'Testimony', 'nhrd' ),
        'description'           => __( 'Customer testimonial', 'nhrd' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-quote',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'testimonials', $args );

}
add_action( 'init', 'nhrd_testimonials', 0 );