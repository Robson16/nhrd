<?php

// Theme Custom Shortcodes

/**
 * 
 */
function get_recent_blog_posts( $atts = array() ) {
    ob_start();
    
    // QTD of post to show
    $qtd = 3;
    if ( is_array( $atts ) && array_key_exists( "qtd", $atts ) ) {
        if ( (int)$atts["qtd"] > 0 ) {
            $qtd = $atts["qtd"];
        }     
    }

    $recentPosts = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => $qtd,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ) );

    echo "<div class='recent-blog-posts'>";
    while ( $recentPosts->have_posts() ) {
        $recentPosts->the_post();
        get_template_part( 'partials/content/content', 'excerpt' );
    }
    echo "</div>";
    
    wp_reset_postdata();

    return ob_get_clean();
}
add_shortcode( 'recent-blog-posts', 'get_recent_blog_posts' );