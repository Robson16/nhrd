<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<div class="container pb-3">
    <div class="row">
        <div class="col-12 col-lg-8">
            <main>
                <?php
                while ( have_posts() ) {
                    the_post(); 

                    get_template_part( 'partials/content/content', 'single' );

                    the_post_navigation(
                        array(
                            'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next Post', 'nhrd' ) . '</span>',
                            'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous Post', 'nhrd' ) . '</span>',
                        )
                    );

                    if (comments_open() || get_comments_number()) comments_template();
                }
                ?>
            </main>
        </div>
        <!-- /.col -->
        <div class="col-12 col-lg-4">
            <?php get_sidebar(); ?>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<!--/.container--> 

<?php
get_footer();